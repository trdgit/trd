# Repository

[https://gitlab.com/trdgit/trd](https://gitlab.com/trdgit/trd)

You should `git clone` using a proxy:
https://gist.github.com/evantoli/f8c23a37eb3558ab8765

# Documentation

## View online

https://trdgit.gitlab.io/trd

## View locally

Install docsify:
`npm install -g docsify-cli`

Serve up the documentation locally or on your shell:
`docsify serve ./manual`
